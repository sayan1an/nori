/*
* Note that MIS is effective when you are sampling using multiple sampling schemes and those schemes overlap in the domain.
*
* MIS weights should follow property that sum of weights must be 1.
* We sample two rays- one from bsdf and one from light sampling.
* Let's say we have 2 light sources. We select the light source with probability c1 and c2 and then sample a ray from the selected source.
* Then combined pdf of light sources = pL = c1 * pL1 + c2 * pL2. Where pL1 and pL2 are probability distribution of source 1 and 2. This is
* anlogous to doing balance heuristics of MIS.
* Then using a similar technique we can combine pdf-bsdf and pdf light.
*/

#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/sampler.h>
#include <nori/warp.h>
#include <nori/emitter.h>
#include <nori/bsdf.h>
#include <nori/Q.h>

NORI_NAMESPACE_BEGIN

/*
 * Known possible source of bug-
 * 1. When the normal at point of intersection and normal at the nearest q-function have different direction, the samples may be wasted if the direction of normal vary greatly. For smooth meshes it should be fine but if you
 * have a non-smooth mesh say a cube, we have problem!! 
 * Possible fix - put a q function at every vertex. The direction of normal at that vertex should be interpolated from the normals of adjacent faces. If you do not interpolate, we will have issues. Another simple way
 * would be to sample q functions so densely that the nearest q-function and point of intersection are on same triangle!!
 * Another soultion would be 
 */

// Warp through delta bsdfs. It may return the same ray if it doesn't hit delta bsdf.
static Ray3f warp(const Scene *scene, Sampler *sampler, const Ray3f &iRay, Color3f &throughput) {
	Ray3f ray = iRay;
	throughput = Color3f(1.0f);

	while (true) {
		Intersection its;
		if (!scene->rayIntersect(ray, its)) {
			return ray;
		}
		const BSDF *bsdf = its.mesh->getBSDF();

		if (!bsdf->isDiffuse()) {
			// Recursively bounce until next diffuse/microfacet surface
			Point2f sample = sampler->next2D();
			BSDFQueryRecord bRec(its.toLocal(-ray.d));
			Color3f weight = bsdf->sample(bRec, sample);
			ray = Ray3f(its.p, its.toWorld(bRec.wo));

			throughput = throughput * weight;
		}
		else {
			return ray;
		}
	}

}

// nextRay is the ray bounced off a diffuse surface going towards the light source.
// It might happend that the nextRay hits a delta bsdf and hits a light source after bouncing through delta-brdfs or hit a diffuse
// surface.
// warpedNextRay gives the nextRay bounced over delta-brdfs if any.
static Color3f sampleLightSource(const Scene *scene, Sampler *sampler, const Ray3f &iRay, Ray3f &nextRay, Ray3f &warpedNextRay) {
	// Find the intersection of the ray with scene.
	Ray3f ray = iRay;
	Color3f throughput(1.0f);
	bool firstBounceOnDiffuseSurface = true;
	nextRay.d = Vector3f(0.0f);
	warpedNextRay = iRay;

	while (true) {
		Intersection its;
		if (!scene->rayIntersect(ray, its)) {
			return Color3f(0.0f);
		}

		if (its.mesh->isEmitter())
			if (its.shFrame.n.dot(-ray.d) > 0)
				return throughput * its.mesh->getEmitter()->getRadiance();
			else
				return Color3f(0.0f);

		const BSDF *bsdf = its.mesh->getBSDF();

		if (!bsdf->isDiffuse()) {
			// Recursively bounce until next diffuse/microfacet surface
			Point2f sample = sampler->next2D();
			// Note that sample.x() is used for dielctric bsdf
			if (sample.y() > 0.95f) {
				return Color3f(0.0f);
			}

			BSDFQueryRecord bRec(its.toLocal(-ray.d));
			Color3f weight = bsdf->sample(bRec, sample);
			ray = Ray3f(its.p, its.toWorld(bRec.wo));
			warpedNextRay = ray;
			throughput = throughput * weight / 0.95f;
		}
		else if (firstBounceOnDiffuseSurface) {
			//Let's uniformly select between N-emitters
			size_t nEmiiters = scene->getEmitterMeshes().size();

			uint32_t emitterIdx = (uint32_t)(sampler->next1D() * nEmiiters);
			const auto &emitter = scene->getEmitterMeshes()[emitterIdx];

			Point3f sampleOnEmiiter;
			Normal3f normalOnEmitter;

			//Sample a point on Emitter surface.
			emitter->samplePosition(sampler->next2D(), sampleOnEmiiter, normalOnEmitter);

			Vector3f wo = sampleOnEmiiter - its.p;
			float distanceFromSource = wo.norm();
			wo /= distanceFromSource;

			float cosineAtSource = normalOnEmitter.dot(-wo);
			// You're looking at the backface of the emiiter
			if (cosineAtSource < 1e-8f)
				return Color3f(0.0f);

			float cosineAtIntersection = its.shFrame.n.dot(wo);
			// This is somewhat redundant since bsdf always checks this condition.
			if (cosineAtIntersection < 1e-8f)
				return Color3f(0.0f);

			BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(wo), ESolidAngle);

			ray = Ray3f(its.p, wo);

			// We don't divide by the pdf here, we will divede by balance heuristic pdf directly later.
			throughput = throughput * bsdf->eval(bRec) * (cosineAtIntersection);

			// From here on four things can happen!!
			// 1. It can hit a light source(Most likely, since we are shooting the ray directly towards light)
			// 2. Hit a diffuse object and return 0 (using the else condition)
			// 3. Hit a delta bsdf object and go on recursion.
			// 4. Escape the scene.(That is impossible since we are shooting directly at a light source but can happen due to numerical issues.)
			// Note: There cannot be a case where we intersect with a light source and then shoot a ray 
			// toward the same light source. This is becasue if the incoming ray hits the light source then it would be captured before entering this if-else block 

			// Return 0 if the ray hits a diffuse source in the next iteration.
			firstBounceOnDiffuseSurface = false;
			nextRay = ray;
			warpedNextRay = ray;
		}
		else {
			return Color3f(0.0f);
		}
	}
}

static float pdfLightSource(const Scene *scene, const Ray3f &ray) {
	Intersection its;
	if (!scene->rayIntersect(ray, its) || !its.mesh->isEmitter())
		return 0;

	// Compute pdf using balance heuristics
	size_t nEmiiters = scene->getEmitterMeshes().size();
	float pEmitter = 1.0f / nEmiiters;
	float pdfSolidAngle = 0;
	for (size_t i = 0; i < nEmiiters; i++)
		pdfSolidAngle += scene->getEmitterMeshes()[i]->pdf(ray);

	return pdfSolidAngle * pEmitter;
}

class RLIntegrator : public Integrator {
public:
	RLIntegrator(const PropertyList &props) {}

	/// Compute the radiance value for a given ray.
	// ray is the camera ray.
	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &iRay) const {
		/* Find the intersection of the ray with scene. */
		Ray3f ray = iRay;
		Color3f throughput(1.0f);
		Color3f accumulate(0.0f);

		Mesh *lastMesh = nullptr;
		Point3f lastPosition;
		Vector3f lastDirection;
		Color3f lastWeight(0.0f);
		bool firstBounce = false; // On first bounce use bsdf sampling and then on use RL sampling

		while (true) {
			Intersection its;
			if (!scene->rayIntersect(ray, its))
				return accumulate;

			if (its.mesh->isEmitter()) {
				Color3f finalReward(0.0f);
				Color3f ret;
				if (its.shFrame.n.dot(-ray.d) > 0) {
					finalReward = lastWeight * its.mesh->getEmitter()->getRadiance();
					ret = accumulate + throughput * its.mesh->getEmitter()->getRadiance();
				}
				else
					ret = accumulate;

				if (lastMesh != nullptr) {
					float scalarReward = std::sqrt(finalReward(0) * finalReward(0) + finalReward(1) * finalReward(1) + finalReward(2) * finalReward(2));
					lastMesh->updateNearbyQ(lastPosition, lastDirection, scalarReward);
				}

				return ret;
			}

			const BSDF *bsdf = its.mesh->getBSDF();
			Color3f weight;
			float terminationProbability = 0.95f;
			Point2f sample = sampler->next2D();

			// Let's check if it should terminate
			if (sample.y() > terminationProbability)
				return accumulate;


			if (!bsdf->isDiffuse()) { // NO MIS FOR Delta bsdf.
				BSDFQueryRecord bRec(its.toLocal(-ray.d));
				weight = bsdf->sample(bRec, sample); // sample.x() is used here!!
				ray = Ray3f(its.p, its.toWorld(bRec.wo));
			}
			else {
				Ray3f nextRay, warpedNextRay;
				Color3f di = sampleLightSource(scene, sampler, ray, nextRay, warpedNextRay);
				float reward = std::sqrt(di(0) * di(0) + di(1) * di(1) + di(2) * di(2)); 
				int32_t token = -1;
				if (!di.isZero()) {
					// If the ray hits the light source directly or through delta-bsdfs only, without intersecting with any diffuse surface,
					// in that case MIS is meaningless as no ray is sampled. In such cases nextRay.d is zero.
					if (nextRay.d.isZero()) {
						accumulate += throughput * di;
					}
					else {
						float pdfB = 0;
						if (firstBounce) {
							BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(nextRay.d), ESolidAngle);
							pdfB = bsdf->pdf(bRec);
						}
						else {
							pdfB = its.mesh->pdfQ(its.p, token, nextRay.d);
						}

						float pdfL = pdfLightSource(scene, warpedNextRay);
						reward /= (pdfB + pdfL);
						accumulate += throughput * di / (pdfB + pdfL);
					}
				}
								
				if (lastMesh != nullptr) {
					float scalarWeight = std::sqrt(lastWeight(0) * lastWeight(0) + lastWeight(1) * lastWeight(1) + lastWeight(2) * lastWeight(2));
					float nextValue = its.mesh->integrateNearestQ(its.p, token, -ray.d);
					lastMesh->updateNearbyQ(lastPosition, lastDirection, scalarWeight * (nextValue + reward));
				}

				float pdfB = 0;
				if (firstBounce) {
					// We will use bsdf-ray to recurse
					BSDFQueryRecord bRec(its.toLocal(-ray.d));
					weight = bsdf->sample(bRec, sampler->next2D());
					
					ray = Ray3f(its.p, its.toWorld(bRec.wo));
					if (bsdf->isMicrofacet())
						pdfB = bRec.pdf;
					else
						pdfB = bsdf->pdf(bRec);

					if (pdfB < 1e-8f) //No point in recursing when weight is zero.
						return accumulate;
				}
				else {
					Vector3f wo;
					its.mesh->sampleQ(sampler->next2D(), its.p, token, wo, pdfB);
					float cosineAtIntersection = its.shFrame.n.dot(wo);
					
					BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(wo), ESolidAngle);

					weight = bsdf->eval(bRec) * (cosineAtIntersection) / pdfB;

					ray = Ray3f(its.p, wo);
					if (pdfB < 1e-8f) //No point in recursing when weight is zero.
						return accumulate;

					//std::cout << pdfB << std::endl;
					if (std::isnan(pdfB) || std::isinf(pdfB))
						std::cout << "Problem with pdf evaluation" << pdfB << std::endl;
				}
				lastMesh = its.mesh;
				lastPosition = its.p;
				lastDirection = ray.d;
				
				// One problem here is that if the sampled direction does not hit a light source but hits a delta bsdf and then hits a light source, in that case pdfLight would be zero.
				// this will cause higher variance. So to solve this problem we warp in through the delta-bsdfs until we reach a non-delta surface.
				Color3f warpTp;
				ray = warp(scene, sampler, ray, warpTp);
				weight *= warpTp;
				// Balance Heuristic
				weight *= pdfB / (pdfB + pdfLightSource(scene, ray));
				lastWeight = weight;
				firstBounce = false;
			}
			throughput = throughput * weight / terminationProbability;
		}
	}

	/// Return a human-readable description for debugging purposes
	std::string toString() const {
		return tfm::format(
			"RL Integrator[\n"
			"  myProperty = \"%s\"\n"
			"]", "Path tracing with two sample per bounce, indirect sample using RL Q function."
		);
	}
};

NORI_REGISTER_CLASS(RLIntegrator, "rl");
NORI_NAMESPACE_END