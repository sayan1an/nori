/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/bsdf.h>
#include <nori/frame.h>

NORI_NAMESPACE_BEGIN

/// Ideal dielectric BSDF
class Dielectric : public BSDF {
public:
    Dielectric(const PropertyList &propList) {
        /* Interior IOR (default: BK7 borosilicate optical glass) */
        m_intIOR = propList.getFloat("intIOR", 1.5046f);

        /* Exterior IOR (default: air) */
        m_extIOR = propList.getFloat("extIOR", 1.000277f);
    }

    Color3f eval(const BSDFQueryRecord &) const {
        /* Discrete BRDFs always evaluate to zero in Nori */
        return Color3f(0.0f);
    }

    float pdf(const BSDFQueryRecord &) const {
        /* Discrete BRDFs always evaluate to zero in Nori */
        return 0.0f;
    }

	Color3f sample(BSDFQueryRecord &bRec, const Point2f &sample) const {
		float cosThetaI = Frame::cosTheta(bRec.wi);
		float rho = 1;
		if (cosThetaI >= 0) { // wi is in m_extIOR
			rho = fresnel(cosThetaI, m_extIOR, m_intIOR);
			if (sample.x() < rho) {// Reflect
				bRec.wo = Vector3f(
					-bRec.wi.x(),
					-bRec.wi.y(),
					bRec.wi.z()
				);
				// Relative index of refraction: no change
				bRec.eta = 1.0f;
			}
			else {
				// Relative index of refraction
				bRec.eta = m_extIOR / m_intIOR;
				float temp = 1.0f - bRec.eta * bRec.eta * (1.0f - cosThetaI * cosThetaI);
				temp = sqrt(temp);
				bRec.wo = Vector3f(
					-bRec.wi.x() * bRec.eta,
					-bRec.wi.y() * bRec.eta,
					-temp//-(bRec.wi.z() - cosThetaI) * bRec.eta - temp
				).normalized();
			}
		}
		else { // wi is in m_intIOR
			cosThetaI *= -1.0;
			rho = fresnel(cosThetaI, m_intIOR, m_extIOR);

			if (sample.x() < rho) {// Reflect
				bRec.wo = Vector3f(
					-bRec.wi.x(),
					-bRec.wi.y(),
					bRec.wi.z()
				);
				// Relative index of refraction: no change 
				bRec.eta = 1.0f;
			}
			else {
				// Relative index of refraction
				bRec.eta = m_intIOR / m_extIOR;
				float temp = 1.0f - bRec.eta * bRec.eta * (1.0f - cosThetaI * cosThetaI);
				temp = sqrt(temp);
				bRec.wo = Vector3f(
					-bRec.wi.x() * bRec.eta,
					-bRec.wi.y() * bRec.eta,
					temp//-(bRec.wi.z() + cosThetaI) * bRec.eta + temp
				).normalized();
				
				//std::cout << bRec.wi.x() << " " << bRec.wi.y() << " " << bRec.wi.z() << " O " << bRec.wo.x() << " " << bRec.wo.y() << " " << bRec.wo.z() << std::endl;
			}
		}

		bRec.measure = EDiscrete;
		// Why do we return 1.0f not 1.0f/rho? This is because we are not trying to estimate an integral on this bounce of
		// light. The integral is already folded into a delta function.
		return Color3f(1.0f);
	}

	// Perfect reflection
	inline Vector3f reflect(const Vector3f &wi) const {
		return Vector3f(-wi.x(), -wi.y(), wi.z());
	}

	bool isDelta() const { 
		return true; 
	}

    std::string toString() const {
        return tfm::format(
            "Dielectric[\n"
            "  intIOR = %f,\n"
            "  extIOR = %f\n"
            "]",
            m_intIOR, m_extIOR);
    }
private:
    float m_intIOR, m_extIOR;
};

NORI_REGISTER_CLASS(Dielectric, "dielectric");
NORI_NAMESPACE_END
