/*
 * Here we sample a single ray along the path and use MIS weights.
 */

#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/sampler.h>
#include <nori/warp.h>
#include <nori/emitter.h>
#include <nori/bsdf.h>
#include <nori/intDirect.h>

NORI_NAMESPACE_BEGIN
class MISIntegrator1 : public Integrator {
public:
	MISIntegrator1(const PropertyList &props){}

	/// Compute the radiance value for a given ray.
	// ray is the camera ray.
	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &iRay) const {
		/* Find the intersection of the ray with scene. */
		Ray3f ray = iRay;
		Color3f throughput(1.0f);
		Color3f accumulate(0.0f);
		
		while (true) {
			Intersection its;
			if (!scene->rayIntersect(ray, its))
				return accumulate;

			if (its.mesh->isEmitter()) {
				if (its.shFrame.n.dot(-ray.d) > 0)
						return accumulate + throughput * its.mesh->getEmitter()->getRadiance();
			}
		
			const BSDF *bsdf = its.mesh->getBSDF();
			Color3f weight;
			float misWeight = 1;
			float terminationProbability = 1.0;
			Point2f sample = sampler->next2D();

			// Let's check if it should terminate
			if (sample.y() > terminationProbability)
					return accumulate;

			
			if (!bsdf->isDiffuse()) { // NO MIS FOR Delta bsdf.
				BSDFQueryRecord bRec(its.toLocal(-ray.d));
				weight = bsdf->sample(bRec, sample); // sample.x() is used here!!
				ray = Ray3f(its.p, its.toWorld(bRec.wo));
			} 
			else { // Go MIS
				float pdfNumerator;
				Vector3f wo;

				// We will draw a single sample.
				size_t nPDFs = scene->getEmitterMeshes().size() + 1; // No. of pdfs is no. of emitters + 1 bsdf.
				
				// Uniform probability of sampling a pdf.
				float pSelect = 1.0f / nPDFs;

				size_t whichPdf = (size_t)(nPDFs * sample.x()); // sample.y() is already used.

				if (whichPdf == nPDFs - 1) { // Sample BSDF
					BSDFQueryRecord bRec(its.toLocal(-ray.d));
					weight = bsdf->sample(bRec, sampler->next2D()) / pSelect;
					wo = its.toWorld(bRec.wo);

					if (bsdf->isMicrofacet())
						pdfNumerator = bRec.pdf;
					else
						pdfNumerator = bsdf->pdf(bRec);
				}
				else { // Sample Light source
					auto emitterMesh = scene->getEmitterMeshes()[whichPdf];
					Point3f sampleOnEmiiter;
					Normal3f normalOnEmitter;

					//Sample a point on Emitter surface.
					float pdfInv = emitterMesh->samplePosition(sampler->next2D(), sampleOnEmiiter, normalOnEmitter);

					wo = sampleOnEmiiter - its.p;
					float distanceFromSource = wo.norm();
					wo /= distanceFromSource;

					float cosineAtSource = std::abs(normalOnEmitter.dot(-wo)); // If it is negetive it should be rejected in the next iteration when it hits light source. If zero then weight is zero and recursion stops
					float cosineAtIntersection = its.shFrame.n.dot(wo); // If this is zero then the weight is zero. Hence recursion stops since we are only shooting one ray.
					if (cosineAtIntersection < 1e-8f || cosineAtSource < 1e-8f || distanceFromSource < 1e-8f)
						return accumulate;
					
					BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(wo), ESolidAngle);
					
					float invPdfSolidAngle = (pdfInv * cosineAtSource) / (distanceFromSource * distanceFromSource);
					weight = bsdf->eval(bRec) * (cosineAtIntersection * invPdfSolidAngle / pSelect);

					pdfNumerator = 1.0f / invPdfSolidAngle;
				}

				float denominator = pdfNumerator;
				Ray3f newRay(its.p, its.toWorld(wo));
				for (size_t i = 0; i < nPDFs; ++i) {
					if (whichPdf != i) {
						if (i == nPDFs - 1) { // bsdf pdf
							BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(wo), ESolidAngle);
							denominator += bsdf->pdf(bRec);
						}
						else {
							auto emitterMesh = scene->getEmitterMeshes()[i];
							denominator += emitterMesh->pdf(newRay);

						}
					}
				}

				misWeight = pdfNumerator / denominator;
				ray = newRay;
			}
			throughput = throughput * misWeight * weight / terminationProbability;
			
		}
	}

	/// Return a human-readable description for debugging purposes
	std::string toString() const {
		return tfm::format(
			"MIS Integrator[\n"
			"  myProperty = \"%s\"\n"
			"]", "Path tracing with one sample per bounce."
		);
	}
};

NORI_REGISTER_CLASS(MISIntegrator1, "mis-1");
NORI_NAMESPACE_END