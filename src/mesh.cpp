/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

	Edited by Sayantan Datta Copyright(c) 2018.
*/

#include <nori/mesh.h>
#include <nori/bbox.h>
#include <nori/bsdf.h>
#include <nori/emitter.h>
#include <nori/warp.h>
#include <Eigen/Geometry>
// Added by sayantan
#include <nori/accel.h>
#include <random>
#include <nanoflann/nanoflann.hpp>
#include <tuple>
#include <fstream>

NORI_NAMESPACE_BEGIN

Mesh::Mesh() { }

Mesh::~Mesh() {
	dumpToFileVisualize();
	dumpToFile();
	delete m_bsdf;
    delete m_emitter;
}

void Mesh::activate() {
	if (!m_bsdf) {
		/* If no material was assigned, instantiate a diffuse BRDF */
		m_bsdf = static_cast<BSDF *>(
			NoriObjectFactory::createInstance("diffuse", PropertyList()));
	}
	accel = nullptr;
	tree = nullptr;
	// Added by Sayantan
	if (m_emitter) {
		m_dpdf.reserve(getTriangleCount());
		for (uint32_t i = 0; i < getTriangleCount(); i++)
			m_dpdf.append(surfaceArea(i));
		// Stores 1/pdf
		m_ipdf = m_dpdf.normalize();

		accel = new Accel();
		((Accel *)accel)->addMesh(this);
		((Accel *)accel)->build();
	}
	// For RL integrator
	else if (m_bsdf->isDiffuse()) {
		if (!loadFromFile()) {
			// Number of samples per unit area
			float sampleDensity = 10;
			//Search radius, i.e. don't let the points too close to each other.
			float searchRadius = 0.1f / std::sqrt(sampleDensity);

			// build a local dynamic kd-tree for maintaining uniform sampling density.
			PointCloud localCloud;
			kd_tree_dynamic dynamic_tree(3, localCloud, nanoflann::KDTreeSingleIndexAdaptorParams(10)); // 10 is max leaf nodes

			float totalSurfaceArea = 0;
			m_dpdf.reserve(getTriangleCount());
			for (uint32_t i = 0; i < getTriangleCount(); i++) {
				float area = surfaceArea(i);
				m_dpdf.append(area);
				totalSurfaceArea += area;
			}
			std::cout << "Total mesh surface area:" << totalSurfaceArea << std::endl;
			m_ipdf = m_dpdf.normalize();

			std::default_random_engine generator;
			std::uniform_real_distribution<float> distribution(0.0, 1.0);

			uint32_t nSamples = (uint32_t)std::round(totalSurfaceArea * sampleDensity) + 1;

			Qfunc.reserve(nSamples);
			float difficulty = 0;
			for (uint32_t i = 0; i < nSamples;) {
				Point2f uv;
				uint32_t index;
				samplePosition(Point3f(distribution(generator), distribution(generator), distribution(generator)), uv, index);
				Point3f point = uvToPoint3f(uv, index);
				std::vector<std::pair<size_t, float>> indices_dists;
				nanoflann::RadiusResultSet<float, size_t> resultSet(searchRadius, indices_dists);
				dynamic_tree.findNeighbors(resultSet, point.data(), nanoflann::SearchParams());

				if (resultSet.size() == 0) {
					localCloud.pts.push_back(point);
					dynamic_tree.addPoints(localCloud.pts.size() - 1, localCloud.pts.size());
					Qfunc.push_back(std::make_tuple(index, uv, std::unique_ptr<Q>(new Q())));
					i++;
				}

				// Just give up if the loop runs too many times!!
				if (difficulty / i > 100.0f)
					break;
				difficulty = difficulty + 1;
			}
			std::cout << "No. of Q functs attached:" << Qfunc.size() << std::endl;
		}

		// Initialize pointCloud
		for (uint32_t i = 0; i < Qfunc.size(); i++) {
			pointCloud.Qref.push_back((*std::get<2>(Qfunc[i])));
			Point3f point = uvToPoint3f(std::get<1>(Qfunc[i]), std::get<0>(Qfunc[i]));
			pointCloud.pts.push_back(point);
			// The tangent direction in the matrial frame must remain same. uvToFrame assigns the tangents such that it is the property of material.
			pointCloud.localFrame.push_back(uvToFrame(std::get<1>(Qfunc[i]), std::get<0>(Qfunc[i])));
		}

		tree = new kd_tree_static(3, pointCloud, nanoflann::KDTreeSingleIndexAdaptorParams(5));
		tree->buildIndex();

		// Genrate approx bsdf
		// Iterate over incoming rays(theta only)
		for (uint32_t k = 0; k < nRows; k++) {
			bsdf[k] = Eigen::Array<float, nRows, nColoumns>::Zero();
			float minTheta = M_PI * k / (2.0f * nRows);
			float maxTheta = M_PI * (k + 1) / (2.0f * nRows);
			
			bsdf[k] = (fitBsdf(minTheta) + fitBsdf(maxTheta) + fitBsdf((maxTheta + minTheta) / 2)) * M_PI * M_PI / (3 * nRows * nColoumns);
		}

		//dumpToFileVisualize();

	}
}

const Eigen::ArrayXXf Mesh::fitBsdf(float theta_in) const {
	Vector3f wi(sin(theta_in), 0, cos(theta_in));
	std::default_random_engine generator;
	std::uniform_real_distribution<float> distribution(0.0, 1.0);
	uint32_t nSamples = 10;

	Eigen::Array<float, nRows, nColoumns> ret = Eigen::Array<float, nRows, nColoumns>::Zero();
	
	for (uint32_t i = 0; i < nRows; i++) {
		float minTheta = M_PI * i / (2.0f * nRows);
		float maxTheta = M_PI * (i + 1) / (2.0f * nRows);
		
		for (uint32_t j = 0; j < nColoumns; j++) {
			float minPhi = 2.0f * M_PI * j / (nColoumns);
			float maxPhi = 2.0f * M_PI * (j + 1) / (nColoumns);
			
			for (uint32_t k = 0; k < nSamples; k++) {
				float phi = minPhi + (maxPhi - minPhi) * distribution(generator);
				float theta = minTheta + (maxTheta - minTheta) * distribution(generator);

				BSDFQueryRecord bRec(wi, Vector3f(sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta)), ESolidAngle);
				Color3f bVal = m_bsdf->eval(bRec);
				if (!bVal.isValid())
					std::cout << "Invalid bsdf" << bVal << std::endl;
				ret(i, j) += std::cos(theta) * std::sin(theta) * std::sqrt(bVal(0) * bVal(0) + bVal(1) * bVal(1) + bVal(2) * bVal(2));
			}

			ret(i, j) /= nSamples;
		}
	}

	return ret;
}

void Mesh::getIndexOfNearestQ(const Point3f &p, int32_t &index) {
	// do a knn search
	if (tree != nullptr && index == -1) {
		const size_t nResults = 1;
		size_t retIndex;
		float out_dist_sqr; // unused
		nanoflann::KNNResultSet<float> resultSet(nResults);
		resultSet.init(&retIndex, &out_dist_sqr);
		tree->findNeighbors(resultSet, p.data(), nanoflann::SearchParams(10));
		index = (int32_t)retIndex;
	}
	else if (tree == nullptr)
		index = -1;
}

float Mesh::integrateNearestQ(const Point3f &p, int32_t &index, const Vector3f &w) {
	// Note that bsdf do not depend on local frame we chhose.
	getIndexOfNearestQ(p, index);
	if (index != -1) {
		float cosThetaCamRay = pointCloud.localFrame[index].toLocal(w).z();
		if (cosThetaCamRay < 1e-7f)
			return 0.0f;
		uint32_t nTheta = (uint32_t)(nRows * 2.0f * INV_PI * std::acos(cosThetaCamRay));
		Q &q = pointCloud.Qref[index];
		return q.integrate(bsdf[nTheta]);
	}
	else {
		throw NoriException("Mesh::sampleQ cannot be called on non-diffuse or emitter meshes!");
	}
}

void Mesh::sampleQ(const Point2f &sample, const Point3f &p, int32_t &index, Vector3f &w, float &pdf) {
	getIndexOfNearestQ(p, index);
	if (index != -1) {
		Q &q = pointCloud.Qref[index];
		float theta, phi;
		pdf = q.sample(sample, theta, phi);
		w = pointCloud.localFrame[index].toWorld(Vector3f(sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta)));
	}
	else {
		throw NoriException("Mesh::sampleQ cannot be called on non-diffuse or emitter meshes!");
	}
}

float Mesh::pdfQ(const Point3f &p, int32_t &index, const Vector3f &w) {
	getIndexOfNearestQ(p, index);
	if (index != -1) {
		Q &q = pointCloud.Qref[index];
		Vector3f _w = pointCloud.localFrame[index].toLocal(w);
		float theta, phi;
		Frame::directionToAngle(_w, theta, phi);
		if (theta >= M_PI / 2 - 1e-4f)
			return 0;
		return q.pdf(theta, phi);
	}
	else {
		throw NoriException("Mesh::sampleQ cannot be called on non-diffuse or emitter meshes!");
	}
}

void Mesh::updateNearbyQ(const Point3f &p, const Vector3f &w, float target) {
	// Each Q function must be updated atomically
	// Assume input w in world frame.
	// Note that w is chosen in world frame becasue the reference frame where the ray intersected and the reference frame of the position where Q is stored can be different.
	if (tree != nullptr) {
		const size_t nResults = 5;
		size_t retIndex[nResults];
		float distanceSq[nResults];
		nanoflann::KNNResultSet<float> resultSet(nResults);
		resultSet.init(retIndex, distanceSq);
		tree->findNeighbors(resultSet, p.data(), nanoflann::SearchParams(10));
		for (uint32_t i = 0; i < resultSet.size(); i++) {
			Vector3f _w = pointCloud.localFrame[retIndex[i]].toLocal(w);
			float theta, phi;
			Frame::directionToAngle(_w, theta, phi);
			if (theta >= M_PI / 2 - 1e-4f)
				continue;
			Q &q = pointCloud.Qref[retIndex[i]];
			q.update(theta, phi, target * std::exp(-10.0f*distanceSq[i]));
		}
	}
	else {
		throw NoriException("Mesh::sampleQ cannot be called on non-diffuse or emitter meshes!");
	}
}

void Mesh::dumpToFileVisualize() const {
	if (tree == nullptr) return;
	uint32_t maxSamples = 100;
	maxSamples = pointCloud.pts.size() < maxSamples ? (uint32_t)pointCloud.pts.size() : maxSamples;
	//All points should be at least searchRadius unit apart
	float searchRadius = 1;

	PointCloud localCloud;
	kd_tree_dynamic dynamic_tree(3, localCloud, nanoflann::KDTreeSingleIndexAdaptorParams(10));

	std::string path("../../ThreeJS/");
	std::ofstream file;
	file.open(path + m_name + ".vis", std::ios::out | std::ios::binary | std::ios::trunc);
	std::vector<float> buffer;
	buffer.push_back((float)nRows);
	buffer.push_back((float)nColoumns);
	float tempArray[nRows * nColoumns];

	for (uint32_t i = 0; i < maxSamples; i++) {
		Point3f p = pointCloud.pts[i];

		std::vector<std::pair<size_t, float>> indices_dists;
		nanoflann::RadiusResultSet<float, size_t> resultSet(searchRadius, indices_dists);
		dynamic_tree.findNeighbors(resultSet, p.data(), nanoflann::SearchParams());

		if (resultSet.size() == 0) {
			localCloud.pts.push_back(p);
			dynamic_tree.addPoints(localCloud.pts.size() - 1, localCloud.pts.size());

			//Write to file
			Eigen::Quaternionf quat = pointCloud.localFrame[i].toQuat();
			//std::cout << "s:" << pointCloud.localFrame[i].s << std::endl;
			//std::cout << "t:" << pointCloud.localFrame[i].t << std::endl;
			//std::cout << "n:" << pointCloud.localFrame[i].n << std::endl;
			buffer.push_back(quat.x());
			buffer.push_back(quat.y());
			buffer.push_back(quat.z());
			buffer.push_back(quat.w());
			file.write((const char *)buffer.data(), buffer.size() * sizeof(float));
			file.write((const char *)p.data(), 3 * sizeof(float));
			buffer.clear();

#if 0
			Vector3f _w = pointCloud.localFrame[i].toLocal(Vector3f(0, 1, 0).normalized());
			float theta, phi;
			Frame::directionToAngle(_w, theta, phi);
			if (theta > M_PI / 2)
				theta = M_PI / 2 - 1e-4f;
			int32_t nTheta = (uint32_t)(nRows * 2.0f * INV_PI * theta);
			int32_t nPhi = (uint32_t)(nColoumns * INV_PI * phi / 2.0f);
			for (int32_t i = 0; i < nRows; i++)
				for (int32_t j = 0; j < nColoumns; j++) {
					if (nTheta - 2 <= i && i <= nTheta + 2 && nPhi - 2 <= j && j <= nPhi + 2)
						tempArray[i * nColoumns + j] = 1.0f;
					else
						tempArray[i * nColoumns + j] = 0.0f;
				}
#else
			//for (uint32_t j = 0; j < nRows * nColoumns; j++)
				//tempArray[j] = std::cos(std::floor(j / nColoumns) / nRows * (M_PI / 2));

			//for (uint32_t i = 0; i < nRows; i++)
				//for (uint32_t j = 0; j < nColoumns; j++) {
					//tempArray[i * nColoumns + j] = bsdf[1](i, j);
				//}
#endif
			
			Q &q = pointCloud.Qref[i];
			q.copyDataForVisualization(tempArray);
			file.write((const char *)tempArray, sizeof(float) * nRows * nColoumns);
		}

	}

	file.close();

	/*for (uint32_t i = 0; i < Qfunc.size(); i++) {
		uint32_t index = std::get<0>(Qfunc[i]);
		std::cout << index << " " << surfaceArea(index)<< std::endl;
	}*/


	
}

void Mesh::dumpToFile() const {
	if (tree == nullptr) return;
	std::string path("");
	std::ofstream file;
	file.open(path + m_name + ".bin", std::ios::out | std::ios::binary | std::ios::trunc);
	
	float tempArray[nRows * nColoumns];
	size_t size = Qfunc.size();
	file.write((const char *)&size, sizeof(size_t));

	for (uint32_t i = 0; i < Qfunc.size(); i++) {
		uint32_t index = std::get<0>(Qfunc[i]);
		Point2f uv = std::get<1>(Qfunc[i]);

		Q &q = (*std::get<2>(Qfunc[i]));
		file.write((const char *)&index, sizeof(uint32_t));
		file.write((const char *)&uv.x(), sizeof(float));
		file.write((const char *)&uv.y(), sizeof(float));
		q.copyRawData(tempArray);
		file.write((const char *)tempArray, sizeof(float) * nRows * nColoumns);
	}

	file.close();
}

bool Mesh::loadFromFile() {
	std::string path("");
	std::ifstream file;
	file.open(path + m_name + ".bin", std::ios::in | std::ios::binary);
	if (file.is_open()) {
		float tempArray[nRows * nColoumns];
		size_t size;
		file.read((char *)&size, sizeof(size_t));

		for (uint32_t i = 0; i < size; i++) {
			uint32_t index;
			float uvx;
			float uvy;
			file.read((char *)&index, sizeof(uint32_t));
			file.read((char *)&uvx, sizeof(float));
			file.read((char *)&uvy, sizeof(float));
			file.read((char *)tempArray, sizeof(float) * nRows * nColoumns);
			Qfunc.push_back(std::make_tuple(index, Point2f(uvx, uvy), std::unique_ptr<Q>(new Q(tempArray))));
		}
		file.close();
		std::cout << "No. of Q functs attached:" << Qfunc.size() << std::endl;
		return true;
	}

	std::cout << "Failed to open file - " << (path + m_name + ".dat") << std::endl;
	return false;
}

Point3f Mesh::uvToPoint3f(const Point2f &uv, const uint32_t index) const {
	Vector3f bary;
	bary << 1 - uv.sum(), uv;
	uint32_t i0 = m_F(0, index), i1 = m_F(1, index), i2 = m_F(2, index);
	const Point3f p0 = m_V.col(i0), p1 = m_V.col(i1), p2 = m_V.col(i2);
	return (p0 * bary.x() + p1 * bary.y() +
		p2 * bary.z());
}

Frame Mesh::uvToFrame(const Point2f &uv, const uint32_t index) const {
	Vector3f bary;
	bary << 1 - uv.sum(), uv;
	uint32_t i0 = m_F(0, index), i1 = m_F(1, index), i2 = m_F(2, index);
	const Point3f p0 = m_V.col(i0), p1 = m_V.col(i1), p2 = m_V.col(i2);
	const Point3f p = (p0 * bary.x() + p1 * bary.y() +
		p2 * bary.z());
	Normal3f n;
	if (m_N.size() > 0) {
		// Interpolated normal
		const Point3f n0 = m_N.col(i0), n1 = m_N.col(i1), n2 = m_N.col(i2);
		n = (n0 * bary.x() + n1 * bary.y() +
			n2 * bary.z()).normalized();
	}
	else
		n = ((p1 - p0).cross(p2 - p0)).normalized();
	Vector3f t = (p0 - p).normalized();
	Vector3f s = (t.cross(n)).normalized();

	return Frame(s, t, n);
}

void Mesh::samplePosition(const Point3f &sample, Point2f &uv, uint32_t &index) const {
	index = (uint32_t)m_dpdf.sample(sample.y());
	float sample1 = sqrt(sample.x());
	uv.x() = 1.0f - sample1;
	uv.y() = sample1 * sample.z();
}

// Method edited by Sayantan, returns pdf of mesh.
float Mesh::samplePosition(const Point2f &sample, Point3f &p, Normal3f &n) const {
	if (m_emitter) {
		float sample1 = sample.x();
		size_t index = m_dpdf.sampleReuse(sample1);
		uint32_t i0 = m_F(0, index), i1 = m_F(1, index), i2 = m_F(2, index);
		sample1 = sqrt(sample1);
		const Point3f p0 = m_V.col(i0), p1 = m_V.col(i1), p2 = m_V.col(i2);

		p = p0 * (1.0f - sample1) + p1 * sample1 * sample.y() +
			p2 * sample1 * (1.0f - sample.y());

		if (m_N.size() > 0) {
			// Interpolated normal
			const Point3f n0 = m_N.col(i0), n1 = m_N.col(i1), n2 = m_N.col(i2);
			n = (n0 * (1.0f - sample1) + n1 * sample1 * sample.y() +
				n2 * sample1 * (1.0f - sample.y())).normalized();
		}
		else
			n = ((p1 - p0).cross(p2 - p0)).normalized();

		return m_ipdf;
	}
	else {
		throw NoriException("Mesh::samplePosition cannot be called on non-emitter objects!");
	}
}

// Method added by Sayantan
float Mesh::pdf(const Ray3f &ray) const {
	if (m_emitter) {
		Intersection its;
		if (((Accel*)accel)->rayIntersect(ray, its, false)) {
			float cosineAtSource = its.shFrame.n.dot(-ray.d);
			float pdfSolidAngle = its.t*its.t / (cosineAtSource * m_ipdf);
			if (cosineAtSource > -1e-8f && cosineAtSource < 1e-8f)
				pdfSolidAngle = 0; // This means that ray does not  intersect.
			
			return std::abs(pdfSolidAngle);
		}
		return 0;
	}
	else {
		throw NoriException("Mesh::samplePosition cannot be called on non-emitter objects!");
	}
}


float Mesh::surfaceArea(uint32_t index) const {
    uint32_t i0 = m_F(0, index), i1 = m_F(1, index), i2 = m_F(2, index);

    const Point3f p0 = m_V.col(i0), p1 = m_V.col(i1), p2 = m_V.col(i2);

    return 0.5f * Vector3f((p1 - p0).cross(p2 - p0)).norm();
}

bool Mesh::rayIntersect(uint32_t index, const Ray3f &ray, float &u, float &v, float &t) const {
    uint32_t i0 = m_F(0, index), i1 = m_F(1, index), i2 = m_F(2, index);
    const Point3f p0 = m_V.col(i0), p1 = m_V.col(i1), p2 = m_V.col(i2);

    /* Find vectors for two edges sharing v[0] */
    Vector3f edge1 = p1 - p0, edge2 = p2 - p0;

    /* Begin calculating determinant - also used to calculate U parameter */
    Vector3f pvec = ray.d.cross(edge2);

    /* If determinant is near zero, ray lies in plane of triangle */
    float det = edge1.dot(pvec);

    if (det > -1e-8f && det < 1e-8f)
        return false;
    float inv_det = 1.0f / det;

    /* Calculate distance from v[0] to ray origin */
    Vector3f tvec = ray.o - p0;

    /* Calculate U parameter and test bounds */
    u = tvec.dot(pvec) * inv_det;
    if (u < 0.0 || u > 1.0)
        return false;

    /* Prepare to test V parameter */
    Vector3f qvec = tvec.cross(edge1);

    /* Calculate V parameter and test bounds */
    v = ray.d.dot(qvec) * inv_det;
    if (v < 0.0 || u + v > 1.0)
        return false;

    /* Ray intersects triangle -> compute t */
    t = edge2.dot(qvec) * inv_det;

    return t >= ray.mint && t <= ray.maxt;
}

BoundingBox3f Mesh::getBoundingBox(uint32_t index) const {
    BoundingBox3f result(m_V.col(m_F(0, index)));
    result.expandBy(m_V.col(m_F(1, index)));
    result.expandBy(m_V.col(m_F(2, index)));
    return result;
}

Point3f Mesh::getCentroid(uint32_t index) const {
    return (1.0f / 3.0f) *
        (m_V.col(m_F(0, index)) +
         m_V.col(m_F(1, index)) +
         m_V.col(m_F(2, index)));
}

void Mesh::addChild(NoriObject *obj) {
    switch (obj->getClassType()) {
        case EBSDF:
            if (m_bsdf)
                throw NoriException(
                    "Mesh: tried to register multiple BSDF instances!");
            m_bsdf = static_cast<BSDF *>(obj);
            break;

        case EEmitter: {
                Emitter *emitter = static_cast<Emitter *>(obj);
                if (m_emitter)
                    throw NoriException(
                        "Mesh: tried to register multiple Emitter instances!");
                m_emitter = emitter;
            }
            break;

        default:
            throw NoriException("Mesh::addChild(<%s>) is not supported!",
                                classTypeName(obj->getClassType()));
    }
}

std::string Mesh::toString() const {
    return tfm::format(
        "Mesh[\n"
        "  name = \"%s\",\n"
        "  vertexCount = %i,\n"
        "  triangleCount = %i,\n"
        "  bsdf = %s,\n"
        "  emitter = %s\n"
        "]",
        m_name,
        m_V.cols(),
        m_F.cols(),
        m_bsdf ? indent(m_bsdf->toString()) : std::string("null"),
        m_emitter ? indent(m_emitter->toString()) : std::string("null")
    );
}

std::string Intersection::toString() const {
    if (!mesh)
        return "Intersection[invalid]";

    return tfm::format(
        "Intersection[\n"
        "  p = %s,\n"
        "  t = %f,\n"
        "  uv = %s,\n"
        "  shFrame = %s,\n"
        "  geoFrame = %s,\n"
        "  mesh = %s\n"
        "]",
        p.toString(),
        t,
        uv.toString(),
        indent(shFrame.toString()),
        indent(geoFrame.toString()),
        mesh ? mesh->toString() : std::string("null")
    );
}

NORI_NAMESPACE_END
