/*
	A multi-bounce Path tracer.
	Sayantan Datta Copyright(c) 2018.
*/

#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/sampler.h>
#include <nori/warp.h>
#include <nori/emitter.h>
#include <nori/bsdf.h>
#include <nori/intDirect.h>

NORI_NAMESPACE_BEGIN
// Our ideal sampling pdf should be c1 * pdf-bsdf + c2 * pdf-light. And of course c1 + c2 = 1. And pdf-bsdf and pdf-light are never disjoint.
// However we can force pdf-bsdf and pdf-light to be disjoint by rejecting samples. i.e The indirect rays must not interesct light in case the bsdf is not delta. 
// This however adds some bias to our estimator since the bsdf-sampling scheme cannot shoot rays in certain direction. Ideally one would change the 
// density function, or re-normalize it since the probabilty of shooting rays in the forbidden direction is zero. But since we are not renormalizing
// the bsdf-pdf to account for the forbidden directions, we add some bias to our estimator. Note that bias small if the solid angle subtended by the
// light is small. So if the light is far away then we have smaller bias.

class PathIntegrator : public Integrator {
public:
	PathIntegrator(const PropertyList &props) : di(props) {}

	/// Compute the radiance value for a given ray.
	// ray is the camera ray.
	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &iRay) const {
		/* Find the intersection of the ray with scene. */
		Ray3f ray = iRay;
		Color3f throughput(1.0f);
		Color3f accumulate(0.0f);
		bool lastIntersectionIsDelta = false;
		bool firstIteration = true;
		
		// Save previous state
		Color3f lastThroughput(1.0f);
		Intersection lastIts;

		while (true) {
			bool backupOneStep = false;
			Intersection its;
			if (!scene->rayIntersect(ray, its))
				return accumulate;

			if (its.mesh->isEmitter()) {
				if (lastIntersectionIsDelta || firstIteration) {
					if (its.shFrame.n.dot(-ray.d) > 0)
						return accumulate + throughput * its.mesh->getEmitter()->getRadiance();
					else
						return accumulate;
				}
				else {
					throughput = lastThroughput;
					its = lastIts;
					backupOneStep = true;
				}
			}
			const BSDF *bsdf = its.mesh->getBSDF();
			Color3f weight;
			float terminationProbability;
			Point2f sample = sampler->next2D();

			// Recursively bounce
			if (!bsdf->isDiffuse()) {
				// Note that sample.x() is used in dielctric bsdf
				if (sample.y() > 1.0f)
					return accumulate;
				lastIntersectionIsDelta = true;
				
				terminationProbability = 1.0f;
			}
			else {
				if (!backupOneStep)
					accumulate += throughput * di.Li(scene, sampler, ray);
				if (sample.y() > 1.0f)
					return accumulate;
				lastIntersectionIsDelta = false;
				
				terminationProbability = 1.0f;
			}
			
			BSDFQueryRecord bRec(its.toLocal(-ray.d));
			weight = bsdf->sample(bRec, sampler->next2D());
			ray = Ray3f(its.p, its.toWorld(bRec.wo));
			
			lastThroughput = throughput;
			lastIts = its;
			throughput = throughput * weight / terminationProbability;
			firstIteration = false;
		}
	}

	/// Return a human-readable description for debugging purposes
	std::string toString() const {
		return tfm::format(
			"NormalIntegrator[\n"
			"  myProperty = \"%s\"\n"
			"]", "Path tracing Integrator without mis."
		);
	}
protected:
	DirectIntegrator di;
};

NORI_REGISTER_CLASS(PathIntegrator, "path_simple");
NORI_NAMESPACE_END