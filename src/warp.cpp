/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

	Edited by Sayantan Datta Copyright(c) 2018.
*/

#include <nori/warp.h>
#include <nori/vector.h>
#include <nori/frame.h>

NORI_NAMESPACE_BEGIN

Point2f Warp::squareToUniformSquare(const Point2f &sample) {
    return sample;
}

float Warp::squareToUniformSquarePdf(const Point2f &sample) {
    return ((sample.array() >= 0).all() && (sample.array() <= 1).all()) ? 1.0f : 0.0f;
}

Point2f Warp::squareToTent(const Point2f &sample) {
    throw NoriException("Warp::squareToTent() is not yet implemented!");
}

float Warp::squareToTentPdf(const Point2f &p) {
    throw NoriException("Warp::squareToTentPdf() is not yet implemented!");
}

Point2f Warp::squareToUniformDisk(const Point2f &sample) {
    throw NoriException("Warp::squareToUniformDisk() is not yet implemented!");
}

float Warp::squareToUniformDiskPdf(const Point2f &p) {
    throw NoriException("Warp::squareToUniformDiskPdf() is not yet implemented!");
}

Vector3f Warp::squareToUniformSphere(const Point2f &sample) {
    throw NoriException("Warp::squareToUniformSphere() is not yet implemented!");
}

float Warp::squareToUniformSpherePdf(const Vector3f &v) {
    throw NoriException("Warp::squareToUniformSpherePdf() is not yet implemented!");
}

Vector3f Warp::squareToUniformHemisphere(const Point2f &sample) {
    throw NoriException("Warp::squareToUniformHemisphere() is not yet implemented!");
}

float Warp::squareToUniformHemispherePdf(const Vector3f &v) {
    throw NoriException("Warp::squareToUniformHemispherePdf() is not yet implemented!");
}

Vector3f Warp::squareToCosineHemisphere(const Point2f &sample) {
	float e1 = sample(0);
	float e2 = sample(1);
	float theta = asin(sqrt(e1));
	float phi = 2 * M_PI * e2;
	float x = sin(theta) * cos(phi);
	float y = sin(theta) * sin(phi);
	float z = cos(theta);
	return Vector3f(x, y, z);
}

float Warp::squareToCosineHemispherePdf(const Vector3f &v) {
	return (v(2) > 0 ? v(2) : 0) / M_PI;
}

Vector3f Warp::squareToBeckmann(const Point2f &sample, float alpha) {
	float e1 = sample.x();
	float e2 = sample.y();
	float costheta = 0;
	float sintheta = 0;

	float tantheta = sqrt(alpha * alpha * logf(1.0f - e1) * -1.0f);
	costheta = 1.0f / sqrt(1 + tantheta * tantheta);
	sintheta = tantheta * costheta;

	float phi = 2 * M_PI * e2;
	//std::cout << tantheta << std::endl;
	float cosphi = cos(phi);
	float sinphi = sin(phi);

	// Compute half vector.
	float xh = sintheta * cosphi;
	float yh = sintheta * sinphi;
	float zh = costheta;

	return Vector3f(xh, yh, zh);
}

float Warp::squareToBeckmannPdf(const Vector3f &m, float alpha) {
	float cosine = m.z(); // Costheta
	if (cosine <= 0) return 0.0f;
	float cosine_sq = cosine * cosine;
	float tan_sq = 1.0f / cosine_sq - 1.0f;
	float alpha_sq = alpha * alpha;

	tan_sq /= alpha_sq;
	tan_sq *= -1.0;
	return exp(tan_sq) / (M_PI * cosine_sq * cosine * alpha_sq);
}

NORI_NAMESPACE_END
