/*
	Copyright(c) 2018 by Sayantan Datta
*/
#include "nori/object.h"
#include "nori/scene.h"
#include "nori/emitter.h"

NORI_NAMESPACE_BEGIN

class AreaLight : public Emitter {
public:
	AreaLight(const PropertyList &props) {
		m_radiance = props.getColor("radiance");
	}

	std::string toString() const {
		return "AreaLight[]";
	}

	Color3f getRadiance() const { return m_radiance; }
};

NORI_REGISTER_CLASS(AreaLight, "area");

NORI_NAMESPACE_END