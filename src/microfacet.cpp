/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

	Edited by Sayantan Datta Copyright(c) 2018.
*/

/*
 * Note to self: In this file wi is incoming from camera, wo is going towards the next light source. In PBRT it is 
 * assumed that wi is from light source. 
 */
#include <nori/bsdf.h>
#include <nori/frame.h>
#include <nori/warp.h>

NORI_NAMESPACE_BEGIN

class Microfacet : public BSDF {
public:
    Microfacet(const PropertyList &propList) {
        /* RMS surface roughness */
        m_alpha = propList.getFloat("alpha", 0.1f);

        /* Interior IOR (default: BK7 borosilicate optical glass) */
        m_intIOR = propList.getFloat("intIOR", 1.5046f);

        /* Exterior IOR (default: air) */
        m_extIOR = propList.getFloat("extIOR", 1.000277f);

        /* Albedo of the diffuse base material (a.k.a "kd") */
        m_kd = propList.getColor("kd", Color3f(0.5f));

        /* To ensure energy conservation, we must scale the 
           specular component by 1-kd. 

           While that is not a particularly realistic model of what 
           happens in reality, this will greatly simplify the 
           implementation. Please see the course staff if you're 
           interested in implementing a more realistic version 
           of this BRDF. */
        m_ks = 1 - m_kd.maxCoeff();
    }

	//beckmannG1 term
	float G1(const Vector3f &w, const Vector3f &wh) const {
		float x = wh.dot(w) / w.z();
		if (x <= 0) return 0.0f;
		x = w.z();
		x *= x; // cos^2
		if (x >= (1.0f - 1e-7f))
			return 1.0f;
		x = sqrt(1.0f / x - 1.0f); // tan
		x *= m_alpha;
		x = 1.0f / x;
		if (x >= 1.6f) return 1.0f;
		return (3.535f * x + 2.181f * x * x) / (1.0f + 2.276f*x + 2.577f * x * x);
	}

	float lambda(const Vector3f &w, const float alpha) const {
		float absTanTheta = std::abs(Frame::tanTheta(w));
		if (std::isinf(absTanTheta)) return 0.0f;
		float a = 1.0f / (alpha * absTanTheta);
		if (a >= 1.6f) return 0.0f;
		return (1.0f - 1.259f * a + 0.396f * a * a) / (3.535f * a + 2.181f * a * a);
	}

	float beckmannG(const Vector3f &wi, const Vector3f &wo, const float alpha) const {
		return 1.0f / (1.0f + lambda(wo, alpha) + lambda(wi, alpha));
	}

    /// Evaluate the BRDF for the given pair of directions
    Color3f eval(const BSDFQueryRecord &bRec) const {
		if (bRec.measure != ESolidAngle
			|| bRec.wi.z() <= 1e-7
			|| bRec.wo.z() <= 1e-7)
			return Color3f(0.0f);

		// Get half vector and angles
		Vector3f wh = (bRec.wi + bRec.wo).normalized();
		float cosThetaI = bRec.wi.z();
		float cosThetaO = bRec.wo.z();

		// Fresnel term
		float F = fresnel(wh.dot(bRec.wo), m_extIOR, m_intIOR);

		// Beckman-D term
		float D = Warp::squareToBeckmannPdf(wh, m_alpha);

		// Geometric (masking-shadowing) term
		//float G = G1(bRec.wi, wh) * G1(bRec.wo, wh);
		float G = beckmannG(bRec.wi, bRec.wo, m_alpha);
		// Jacobian of half vector
		float Jh = 1.0f / (4.0f * cosThetaI * cosThetaO * wh.z());

		// Diffuse term + specular term
		return (m_kd * INV_PI) + (m_ks * F * D * G * Jh);
    }

	float pdf(const BSDFQueryRecord &bRec) const {
		if (bRec.measure != ESolidAngle
			|| bRec.wi.z() <= 1e-7
			|| bRec.wo.z() <= 1e-7)
			return 0.0f;

		// Get half vector and angles
		Vector3f wh = (bRec.wi + bRec.wo).normalized();
		float cosThetaO = bRec.wo.z();

		// Beckmann D term
		float D = Warp::squareToBeckmannPdf(wh, m_alpha);

		// Jacobian of the half vector
		float Jh = 1.0f / (4.0f * (wh.dot(bRec.wi)));

		// Diffuse term + specular term
		return ((1.0f - m_ks) * cosThetaO * INV_PI) + (m_ks * D * Jh);
	}

    /// Sample the BRDF
    Color3f sample(BSDFQueryRecord &bRec, const Point2f &_sample) const {
		if (bRec.wi.z() <= 1e-7)
			return Color3f(0.0f);
		bRec.measure = ESolidAngle;

		// Sample specular
		if (_sample.x() < m_ks) {
			Point2f newSample(_sample.x() / m_ks, _sample.y());
			Vector3f wh(Warp::squareToBeckmann(newSample, m_alpha));
			bRec.wo = (2.0f * bRec.wi.dot(wh) * wh - bRec.wi);
		}

		// Sample diffuse
		else {
			Point2f newSample((_sample.x() - m_ks) / (1.0f - m_ks), _sample.y());
			bRec.wo = Warp::squareToCosineHemisphere(newSample);
		}

		// Invisible, Depending on the sampled half angle direction, wo can be below horizon!!
		if (bRec.wo.z() <= 0) return Color3f(0.0f);

		bRec.eta = 1.0f;
		bRec.pdf = pdf(bRec);
		// Follow convention, eval * cosine(angle with light source) / pdf.
		return (bRec.pdf >= 1e-7f) ? eval(bRec) * bRec.wo.z() / bRec.pdf : Color3f(0.0f);
    }
	
    bool isDiffuse() const {
        /* While microfacet BRDFs are not perfectly diffuse, they can be
           handled by sampling techniques for diffuse/non-specular materials,
           hence we return true here */
        return true;
    }

	// Added by sayantan
	bool isMicrofacet() const {
		return true;
	}

    std::string toString() const {
        return tfm::format(
            "Microfacet[\n"
            "  alpha = %f,\n"
            "  intIOR = %f,\n"
            "  extIOR = %f,\n"
            "  kd = %s,\n"
            "  ks = %f\n"
            "]",
            m_alpha,
            m_intIOR,
            m_extIOR,
            m_kd.toString(),
            m_ks
        );
    }
private:
    float m_alpha;
    float m_intIOR, m_extIOR;
    float m_ks;
    Color3f m_kd;
};

NORI_REGISTER_CLASS(Microfacet, "microfacet");
NORI_NAMESPACE_END
