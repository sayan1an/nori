#include "nori/intDirect.h"

NORI_NAMESPACE_BEGIN

/* 
 * Note: There are some differences in the version 1 and 2 of the direct illumination integrator. The end difference is that version
 * 2, after the ray is bounced off a diffuse surface, if it intersects any delta brdf it does not immediately return zero but bounces 
 * furthur until it reaches a light source(jackpot) or another diffuse surface(return zero.).
 * Another difference is that version 2 has higher variance(lower compute cost) since we are only sampling one light source. In version 1, we are sampling all light source.
 * 
 * Let's see how version 1 and 2 are equivalent in terms of unbiased convergence rate of convergence given same compute budget, that
 * is we do not shoot any extra rays. 
 * Ideal equation for version 1.
 * Lets say we have three light sources pL1, pL2, pL3 where pLs are the solid angle probability distribution of light sources.
 * Now for monte-carlo estimation, we cannot sample three rays from three different distribution. All samples must come from a single distribution.
 * Let's say we combine pL1, pL2 and pL3 using three random variables c1, c2, c3 where c1 + c2 + c3 = 1.
 * Then pL = c1 * pL1 + c2 * pL2 + c3 * pL3.
 * How do we sample from pL? Select a light source based on c's and then sample from selected light source.
 * How would our estimator look like F = sum over m samples(f(x) / (c1*pL1(x) + c2*pL2(x) + c3*pL3(x))) / m.
 * Now assume c1 = c2 = c3 and pL1 and pL2 and pL3 are disjoint distributions. This is what version 1 assumes. The c1 = c2 = c3 assumption comes from the fact that we are sampling
 * three rays deterministically from pl1, pl2 and pl3. The disjoint assumption comes from the fact that our denominator contains just one pdf term instead a sum of three. The c and m
 * terms also vanishes because c * m(=3) = 1.
 * Version 2 however does not need to assume c1 = c2 = c3 but for simplicity we do that. The averaging 1/m is done at the camera.
 * In making assumption c1 = c2 = c3 these assumptions we introduce more variance but no bias.
 * But assuming disjoint distribution, we introduce bias.
 * This is where MIS comes in i.e w * f / p1 = f / (p1 + p2 + p3). This is what Balance Heuristics is. So it essentially doing what I just explained above. In that sense MIS
 * is something that a sane person would always do. MIS is not luxury but basic fundamental need whenever you are trying to eastimate an integral with more than one pdf. However 
 * MIS is more gneral than just balance heuristic.
 */

/*
 *  It should also be noted that MIS is useless in version 1. Let's say there are 3 light sources, pdf1 pdf2 pdf3. We sample three
 *  rays for 3 sources. If the pdfs are not overlapping, or pdfs are mutually exclusive then MIS weight is always 1 for all three schemes.
 * 
 *  Same applies for version 2. Although you'd use 1 sample version of MIS estimator, the weight would be 1 if pdfs of two light
 *  sources are mutually exclusive.
 *  Also note that one must not calculate the MIS weight with bsdf sampling since no samples are drawn using bsdf scheme. 
 */

/// Compute the radiance value for a given ray.
// ray is the camera ray.

// This is version 1
/*Color3f DirectIntegrator::Li(const Scene *scene, Sampler *sampler, const Ray3f &iRay) const {
	// Find the intersection of the ray with scene.
	Ray3f ray = iRay;
	Color3f throughput(1.0f);
	while (true) {
		Intersection its;
		if (!scene->rayIntersect(ray, its))
			return Color3f(0.0f);

		if (its.mesh->isEmitter()) {
			if (its.shFrame.n.dot(-ray.d) > 0)
				return throughput * its.mesh->getEmitter()->getRadiance();
			else
				return Color3f(0.0f);
		}
		const BSDF *bsdf = its.mesh->getBSDF();

		if (!bsdf->isDiffuse()) {
			// Recursively bounce until next diffuse/microfacet surface
			Point2f sample = sampler->next2D();
			// Note that sample.x() is used for dielctric bsdf
			if (sample.y() > 0.95f)
				return Color3f(0.0f);

			BSDFQueryRecord bRec(its.toLocal(-ray.d));
			Color3f weight = bsdf->sample(bRec, sample);
			ray = Ray3f(its.p, its.toWorld(bRec.wo));

			throughput = throughput * weight / 0.95f;
		}
		else {
			Color3f retCol(0.0f);
			// Sample the light source if any.
			for (const auto emitterMesh : scene->getEmitterMeshes()) {
				Point3f sampleOnEmiiter;
				Normal3f normalOnEmitter;
				float pdfInv = emitterMesh->samplePosition(sampler->next2D(), sampleOnEmiiter, normalOnEmitter);
				Vector3f wo = sampleOnEmiiter - its.p;
				float distanceFromSource = wo.norm();
				wo /= distanceFromSource;
				float cosineAtSource = normalOnEmitter.dot(-wo);
				// You're looking at the backface of the emiiter
				if (cosineAtSource < 0)
					continue;
				float cosineAtIntersection = its.shFrame.n.dot(wo);
				if (cosineAtIntersection < 0) // This is somewhat redundant since bsdf always checks this condition.
					continue;

				BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(wo), ESolidAngle);

				Ray3f ray_wo(its.p, wo);
				Intersection _its;
				if (!scene->rayIntersect(ray_wo, _its)) {
					//std::cout << "WTF is wrong?! Numerical Error? Sampled direction on light source must intersect with scene. " << std::endl;
					continue;
				}
				// Avoid self intersection and check if the path to light is blocked.
				if (_its.mesh != emitterMesh && _its.t < distanceFromSource)
					continue;

				retCol = retCol + emitterMesh->getEmitter()->getRadiance() * bsdf->eval(bRec) *
					((pdfInv * cosineAtIntersection * cosineAtSource) / (distanceFromSource * distanceFromSource));
			}

			return retCol * throughput;
		}
	}
}*/

// This is version 2.
Color3f DirectIntegrator::Li(const Scene *scene, Sampler *sampler, const Ray3f &iRay) const {
	// Find the intersection of the ray with scene.
	Ray3f ray = iRay;
	Color3f throughput(1.0f);
	bool firstBounceOnDiffuseSurface = true;

	while (true) {
		Intersection its;
		if (!scene->rayIntersect(ray, its))
			return Color3f(0.0f);

		if (its.mesh->isEmitter())
			if (its.shFrame.n.dot(-ray.d) > 0)
				return throughput * its.mesh->getEmitter()->getRadiance();
			else
				return Color3f(0.0f);

		const BSDF *bsdf = its.mesh->getBSDF();

		if (!bsdf->isDiffuse()) {
			// Recursively bounce until next diffuse/microfacet surface
			Point2f sample = sampler->next2D();
			// Note that sample.x() is used for dielctric bsdf
			if (sample.y() > 0.95f)
				return Color3f(0.0f);

			BSDFQueryRecord bRec(its.toLocal(-ray.d));
			Color3f weight = bsdf->sample(bRec, sample);
			ray = Ray3f(its.p, its.toWorld(bRec.wo));

			throughput = throughput * weight / 0.95f;
		}
		else if (firstBounceOnDiffuseSurface) {
			//Let's uniformly select between N-emitters
			size_t nEmiiters = scene->getEmitterMeshes().size();
			float pEmitter = 1.0f / nEmiiters; //Compute the probability of selecting an emitter

			uint32_t emitterIdx = (uint32_t)(sampler->next1D() * nEmiiters);
			const auto &emitter = scene->getEmitterMeshes()[emitterIdx];

			Point3f sampleOnEmiiter;
			Normal3f normalOnEmitter;

			//Sample a point on Emitter surface.
			float pdfInv = emitter->samplePosition(sampler->next2D(), sampleOnEmiiter, normalOnEmitter);

			Vector3f wo = sampleOnEmiiter - its.p;
			float distanceFromSource = wo.norm();
			wo /= distanceFromSource;

			float cosineAtSource = normalOnEmitter.dot(-wo);
			// You're looking at the backface of the emiiter
			if (cosineAtSource < 0)
				return Color3f(0.0f);

			float cosineAtIntersection = its.shFrame.n.dot(wo);
			// This is somewhat redundant since bsdf always checks this condition.
			if (cosineAtIntersection < 0)
				return Color3f(0.0f);

			BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(wo), ESolidAngle);

			ray = Ray3f(its.p, wo);

			float invPdfSolidAngle = (pdfInv * cosineAtSource) / (distanceFromSource * distanceFromSource * pEmitter);
			throughput = throughput * bsdf->eval(bRec) * (cosineAtIntersection * invPdfSolidAngle);
			
			// From here on four things can happen!!
			// 1. It can hit a light source(Most likely since we are shooting the ray directly towards light)
			// 2. Hit a diffuse object and return 0 (using the else condition)
			// 3. Hit a delta bsdf object and go on recursion.
			// 4. Escape the scene.(That is impossible since we are shooting directly at a light source but can happen due to numerical issues.)
			// Note: There cannot be a case where we intersect with a light source and then shoot a ray 
			// toward the same light source. This is becasue if the incoming ray hits the light source then it would be captured before entering this if-else block 
			
			// Return 0 if the ray hits a diffuse source in the next iteration.
			firstBounceOnDiffuseSurface = false;
		}
		else
			return Color3f(0.0f);
	}
}


NORI_REGISTER_CLASS(DirectIntegrator, "direct");
NORI_NAMESPACE_END