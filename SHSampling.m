%Spherical Harmonics Sampling
% Each spherical harmonics basis function is separable as SH(theta, phi) =
% f(theta) * g(phi)

% We will use first nine SH functions
syms theta phi real

i = sqrt(-1)
%First nine SH basis functions
% Y00 = 1 / sqrt(4 * pi)
% Y1_1 = sqrt(3 / (8 * pi)) * sin(theta) * exp(-i * phi)
% Y10 = sqrt(3 / (4 * pi)) * cos(theta)
% Y11 = -sqrt(3 / (8 * pi)) * sin(theta) * exp(i * phi)
% Y2_2 = sqrt(15 / (32 * pi)) * (sin(theta))^2 * exp(-2 * i * phi)
% Y2_1 = sqrt(15 / (8 * pi)) * sin(theta) * cos(theta) * exp(-i * phi)
% Y20 = sqrt(5 / (16 * pi)) * (3 * cos(theta) * cos(theta) - 1)
% Y21 = -sqrt(15 / (8 * pi)) * sin(theta) * cos(theta) * exp(i * phi)
% Y22 = -sqrt(15 / (32 * pi)) * (sin(theta))^2 * exp(2 * i * phi)

%Simplified SH
% We subtract the complex terms to remove complex part
Y00 = 1;
Y10 = cos(theta);
Y11 = sin(theta) * cos(phi);
Y20 = 3 * cos(theta) * cos(theta) - 1;
Y21 = sin(theta) * cos(theta) * cos(phi);
Y22 = (sin(theta))^2 * cos(2*phi);

%First nine SH Coef
syms c0 c1 c2 c3 c4 c5 real
%F = simplify(c0 * Y00 + c1 * Y10 + c2 * (Y1_1 - Y11) + c3 * Y20 + c4 * (Y2_1 - Y21) + c5 * (Y2_2 - Y22)) 
F = simplify(c0 * Y00 + c1 * Y10 + c2 * Y11 + c3 * Y20 + c4 * Y21 + c5 * Y22)
%Integrate between theta=-pi/2 to pi/2 and phi=0 to 2*pi 
NormalizeFactor = int(int(F * sin(theta), theta, 0, pi), phi, 0, 2*pi)

%Find marginal density
p_theta = int(int(F * sin(theta), phi, 0, 2*pi), theta)

p_phi = simplify(int(F * sin(theta) / p_theta, phi))

%plot F
c0 = 1;
c1 = 1;
c2 = 1;
c3 = 1;
c4 = 1;
c5 = 1;
theta = 0:0.05:pi;
phi = 0:0.05:2*pi;

%plot cdf theta
plot(theta, subs(p_theta))

[theta,phi] = meshgrid(theta, phi);
r = double(subs(F));

%Convert to matlab system
theta = theta - pi/2;
[x,y,z] = sph2cart(phi, theta, r);
%surf(double(x),double(y),double(z))

