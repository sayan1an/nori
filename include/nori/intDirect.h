#pragma once

/*
A Direct Illumination Only integrator.
Sayantan Datta Copyright(c) 2018.
*/

#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/sampler.h>
#include <nori/warp.h>
#include <nori/emitter.h>
#include <nori/bsdf.h>

NORI_NAMESPACE_BEGIN

class DirectIntegrator : public Integrator {
public:
	DirectIntegrator(const PropertyList &props) {}

	/// Compute the radiance value for a given ray.
	// ray is the camera ray.
	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &iRay) const;

	/// Return a human-readable description for debugging purposes
	std::string toString() const {
		return tfm::format(
			"NormalIntegrator[\n"
			"  myProperty = \"%s\"\n"
			"]", "Direct Illumination Integrator"
		);
	}
protected:
};
NORI_NAMESPACE_END
