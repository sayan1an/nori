#pragma once
#include <nori/common.h>
#include <nori/frame.h>
#include <nanoflann/nanoflann.hpp>
#include <tbb/mutex.h>
#include <random>

NORI_NAMESPACE_BEGIN
class DiscreteSinusoid {
	Eigen::ArrayXf sinTheta;
	Eigen::ArrayXf cosTheta;
	const uint32_t nSamples = 10;
public:
	DiscreteSinusoid(uint32_t nRows) : sinTheta(Eigen::ArrayXf::Zero(nRows, 1)), cosTheta(Eigen::ArrayXf::Zero(nRows, 1)) {
		std::default_random_engine generator;
		std::uniform_real_distribution<float> distribution(0.0, 1.0);

		for (uint32_t i = 0; i < nRows; i++) {
			float minTheta = M_PI * i / (2.0f * nRows);
			float maxTheta = M_PI * (i + 1) / (2.0f * nRows);

			for (uint32_t k = 0; k < nSamples; k++) {
				float theta = minTheta + (maxTheta - minTheta) * distribution(generator);
				sinTheta(i) += std::sin(theta);
				cosTheta(i) += std::cos(theta);
			}
		}

		sinTheta /= (float)nSamples;
		cosTheta /= (float)nSamples;
	}

	const Eigen::ArrayXf & getSin() const {
		return sinTheta;
	}

	const Eigen::ArrayXf & getCos() const {
		return cosTheta;
	}
};


class Q {
private:
	const static uint32_t nRows = 18;
	const static uint32_t nColoumns = 72;
	static DiscreteSinusoid discreteSinusoid;
	Eigen::Array<float, nRows, nColoumns> q; // theta between 0 to pi/2 and phi from 0 to 2*pi
	tbb::mutex m; // tbb::mutex is not copyable or movable. So this makes the class Q immovable. So we need to use vector of pointers.
	float normalization;
	bool needNormalization;
	void normalize() {
		{
			tbb::mutex::scoped_lock lock(m);
			if (needNormalization) {
				const Eigen::ArrayXf &sin = discreteSinusoid.getSin();
				normalization = 0;
				for (uint32_t i = 0; i < nColoumns; i++)
					normalization += (q.col(i) * sin).sum();
				
				normalization *= M_PI * M_PI / (nRows * nColoumns);
				needNormalization = false;
			}
		}
	}
public:
	Q() {
		q = Eigen::Matrix <float, nRows, nColoumns>::Ones() * 0.001f;
		needNormalization = true;
		normalization = 0.0f;
	}

	Q(const float *array) {
		for (uint32_t i = 0; i < nRows; i++)
			for (uint32_t j = 0; j < nColoumns; j++) {
				q(i, j) = array[i * nColoumns + j];
			}

		needNormalization = true;
		normalization = 0.0f;
	}

	void update(float theta, float phi, float value) {
		uint32_t nTheta = (uint32_t)(nRows * 2.0f * INV_PI * theta);
		uint32_t nPhi = (uint32_t)(nColoumns * INV_PI * phi / 2.0f);

		{	
			tbb::mutex::scoped_lock lock(m);
#if 1
			bool isNotInfYet = true;
			if (std::isinf(q(nTheta, nPhi)) || std::isnan(q(nTheta, nPhi))) {
				isNotInfYet = false;
				std::cout << "Q already inf" << value << " " << q(nTheta, nPhi) << std::endl;
			}
#endif
			//float alpha = 1.0f / (1.0f +  q(nTheta, nPhi));
			float alpha = 0.1f;
			q(nTheta, nPhi) = (1.0f - alpha) * q(nTheta, nPhi) + alpha * value;
			needNormalization = true;
#if 1
			if ((std::isinf(q(nTheta, nPhi)) || std::isnan(q(nTheta, nPhi))) && isNotInfYet)
				std::cout << "Q just inf" << value << " " << q(nTheta, nPhi) << std::endl;
#endif
		}
	}
	
	float pdf(const float theta, const float phi) {
		uint32_t nTheta = (uint32_t)(nRows * 2.0f * INV_PI * theta);
		uint32_t nPhi = (uint32_t)(nColoumns * INV_PI * phi / 2.0f);

		normalize();

		float ret;
		{
			tbb::mutex::scoped_lock lock(m);
			if (normalization < 1e-7)
				ret = INV_PI / 2;
			else
				ret = q(nTheta, nPhi) / normalization;
		}

		if (std::isnan(ret) || std::isinf(ret)) {
			std::cout << "Problem in pdf method in class Q " << q(nTheta, nPhi) << " " << nTheta << " " << nPhi << " " << normalization << std::endl;
		}
		
		return ret;
	}

	float sample(const Point2f &sample, float &theta, float &phi) {
		normalize();

		//Uniform sampling
		// Here is a potential bug. Variable normalization might be changing when executing the following if statement. For now, let's just forget about it.
		if (normalization < 1e-7) {
			theta = M_PI / 2 * sample.x();
			phi = 2 * M_PI * sample.y();
			return INV_PI / 2;
		}

		Eigen::Array<float, nRows, 1> marginalTheta;
		Eigen::Array<float, nRows, 1> sinTheta = discreteSinusoid.getSin();
		{
			tbb::mutex::scoped_lock lock(m);
			marginalTheta = (q.rowwise().sum() * sinTheta) * 2 * M_PI / (normalization * nColoumns);
		}

		//std::cout << marginalTheta.sum() * M_PI / (2 * nRows)  << std::endl; // Should be 1 i.e integral of marginal pdf from 0 to pi/2 is 1.
		float cumulativeTheta = 0;
		uint32_t i;
		float scaledUniformRandom = sample.x() * 2 * nRows / M_PI;
		for (i = 0; i < nRows; i++) {
			// Since computing CDF is equivalent to doing integration and since we are discretizing the integral, we should multiply marginalTheta(i) with M_PI / (2 * nRows).
			// Equivalently one can multiply sample.x() by (2 * nRows) / M_PI;
			cumulativeTheta += marginalTheta(i);
			if (cumulativeTheta > scaledUniformRandom)
				break;
		}
		if (i >= nRows) {
			std::cout << "Problem with q-sampling(theta) " << cumulativeTheta << " " << scaledUniformRandom << std::endl;
			i--;
		}

		uint32_t j;
		float cumulativePhi = 0;
		float ret = 0;

		{
			tbb::mutex::scoped_lock lock(m);
			// Here our integrand is q(theta, phi) * sin(theta) / (marginal(theta) * normalization)
			// Due to discrete setting we multiply integrand by 2 * pi / nColoumns
			// so finally we have sin(theta) * 2 * pi / (marginal(theta) * normalization * nColoumns);
			//std::cout << q.row(i).sum() * sinTheta(i) * 2 * M_PI / (marginalTheta(i) * normalization * nColoumns) << std::endl; // Integral of conditional density or cdf should be 1.
			scaledUniformRandom = sample.y() * marginalTheta(i) * normalization * nColoumns / (sinTheta(i) * 2 * M_PI);
			for (j = 0; j < nColoumns; j++) {
				cumulativePhi += q(i, j);
				if (cumulativePhi > scaledUniformRandom)
					break;
			}

			if (j >= nColoumns) {
				std::cout << "Problem with q-sampling(phi) " << cumulativePhi << " " << scaledUniformRandom << " " << normalization << std::endl;
				j--;
			}

			ret = q(i, j) / normalization;
		}

		

		float minTheta = M_PI * i / (2.0f * nRows);
		float maxTheta = M_PI * (i + 1) / (2.0f * nRows);
		float minPhi = 2.0f * M_PI * j / (nColoumns);
		float maxPhi = 2.0f * M_PI * (j + 1) / (nColoumns);

		theta = minTheta + (maxTheta - minTheta) * sample.y();
		phi = minPhi + (maxPhi - minPhi) * sample.x();

		if (std::isnan(ret) || std::isinf(ret)) {
			std::cout << "Problem in sample method in class Q " << q(i, j) << " " << i << " " << j << " " << normalization << std::endl;
		}
		//if (theta >= M_PI / 2)
			//std::cout << "Theta bigger than pi/2" << std::endl;
		
		return ret;
	}


	float integrate(const Eigen::Array<float, nRows, nColoumns> &bsdf) {
		float ret = 0;
		{	
			tbb::mutex::scoped_lock lock(m);
			ret = (bsdf * q).sum();
		}
		return ret;
	}

	void copyDataForVisualization(float *arr) {
		tbb::mutex::scoped_lock lock(m);
		float min = q.minCoeff();
		float max = q.maxCoeff();
		float mean = q.mean();
		float t = max - min;
		if (std::abs(t) < 1e-7f)
			t = 1;
		float stddev = 0;
		for (uint32_t i = 0; i < nRows; i++)
			for (uint32_t j = 0; j < nColoumns; j++) {
				if (std::isinf(q(i, j))) {
					std::cout << "Q function is inf....fix it" << std::endl;
				}
				stddev += ((q(i, j) - mean) / (nRows * nColoumns)) * (q(i, j) - mean);
				arr[i * nColoumns + j] = (q(i, j) - min);
			}

		stddev = std::sqrt(stddev);

		float normalizeBy = stddev;
		if (stddev < 1e-7f || std::isinf(stddev)) {
			std::cerr << "Stddev of q function is inf!! Normalize using min-max." << std::endl;
			normalizeBy = t;
		}

		for (uint32_t i = 0; i < nRows; i++)
			for (uint32_t j = 0; j < nColoumns; j++)
				arr[i * nColoumns + j] /= normalizeBy;
			
	}

	void copyRawData(float *arr) {
		tbb::mutex::scoped_lock lock(m);

		for (uint32_t i = 0; i < nRows; i++)
			for (uint32_t j = 0; j < nColoumns; j++) {
				arr[i * nColoumns + j] = q(i, j);
			}

	}
};

struct PointCloud
{
	std::vector<Point3f>  pts;
	// Stores reference to Q functions. Actual Q funct with data is stored with mesh.
	std::vector<std::reference_wrapper<Q>> Qref;
	std::vector<Frame> localFrame;

	// Must return the number of data points
	inline size_t kdtree_get_point_count() const { return pts.size(); }

	// Returns the dim'th component of the idx'th point in the class:
	// Since this is inlined and the "dim" argument is typically an immediate value, the
	//  "if/else's" are actually solved at compile time.
	inline float kdtree_get_pt(const size_t idx, int dim) const
	{
		if (dim == 0) return pts[idx].x();
		else if (dim == 1) return pts[idx].y();
		else return pts[idx].z();
	}

	// Optional bounding-box computation: return false to default to a standard bbox computation loop.
	//   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
	//   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
	template <class BBOX>
	bool kdtree_get_bbox(BBOX& /* bb */) const { return false; }
};

typedef nanoflann::KDTreeSingleIndexAdaptor<nanoflann::L2_Simple_Adaptor<float, PointCloud>, PointCloud, 3> kd_tree_static;
typedef nanoflann::KDTreeSingleIndexDynamicAdaptor<nanoflann::L2_Simple_Adaptor<float, PointCloud>, PointCloud, 3> kd_tree_dynamic;
NORI_NAMESPACE_END